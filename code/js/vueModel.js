const imgsrc = "./images/";
const serverurl = "/getdata/shapi/";
const domeurl = "http://127.0.0.1:8020/2019-10-21";
//创建全局的组件index1
Vue.component("shIndex1",{ //test1:组件名
	//不支持ES6语法形式
	template:"#index1",//创建模板
	data:function(){
			return {		//返回对象是为了保证每一个组件对应的值都唯一性
			name:"商行",
		};
	},
	methods:{
	}
});
//创建全局的组件index2
Vue.component("shIndex2",{ 
	//不支持ES6语法形式
	template:"#index2",//创建模板
	data:function(){
			return {		//返回对象是为了保证每一个组件对应的值都唯一性
			name:"商行",
		};
	},
	methods:{
	}
});
//创建全局的组件index3
Vue.component("shIndex3",{ 
	//不支持ES6语法形式
	template:"#index3",//创建模板
	data:function(){
			return {		//返回对象是为了保证每一个组件对应的值都唯一性
			name:"商行",
		};
	},
	methods:{
	}
});
//创建全局的组件index4
Vue.component("shIndex4",{ 
	//不支持ES6语法形式
	template:"#index4",//创建模板
	data:function(){
			return {		//返回对象是为了保证每一个组件对应的值都唯一性
			name:"商行",
		};
	},
	methods:{
	}
});
//创建全局的组件index5
Vue.component("shIndex5",{ 
	//不支持ES6语法形式
	template:"#index5",//创建模板
	data:function(){
			return {		//返回对象是为了保证每一个组件对应的值都唯一性
			name:"商行",
		};
	},
	methods:{
	}
});
//创建全局的组件shLiebiao
Vue.component("shLiebiao",{ 
	//不支持ES6语法形式
	template:"#liebiao",//创建模板
	data:function(){
		return {		//返回对象是为了保证每一个组件对应的值都唯一性
			liebiaoData:[],
		};
	},
	created: function() {
		console.log("创建组件shLiebiao之后",this.$parent.flag);
		axios.get(serverurl+'/getLieBiaoData?type='+this.$parent.type).then((response)=>{
			console.log(response.data);
            this.liebiaoData = response.data;
       	}).catch((response)=>{
            console.log(response);
            alert("获取shLiebiao初始数据失败");
        })
	},
	methods:{
	}
});
//实例化Vue对象
new Vue({
	el: "#myVue", //element需要获取的对象，是vue操作的根容器元素
	data: { //用于数据的存储，
		name: "商行",
		flag: "index",
		type: 1
	},
	created: function() {
		console.log("创建根组件之后");
		if (window.location.hash){
			this.type = (window.location.hash).substring(1);
		}
	},
	methods: {
		getXiangqing: function(v) {
			this.flag = "xiangqing";
			this.type = v;
		},
		getFenlei: function(v) {
			this.flag = "liebiao";
			this.type = v;
		}
	},
	components: {
		"shTop": {
			template: '#top',
			data: function() { //tada必须是一个方法，其返回值为obj对象
				return {
					name: "top",
					Suspension_menu_listL: null,
					hd_menu_list: null,
					Words_list: null,
					Menu_listData: [],
					Navigation_name_list: null,
					shangpingData: []
				};
			},
			created: function() {
				console.log("创建top之后");
				this.getTopData();
				this.getMenu_listData();
				this.getID();
			},
			methods: {
				shangpinxq: function(v) {
					this.$parent.getXiangqing(v);
				},
				fenleiss: function(v) {
					this.$parent.getFenlei(v);
				},
				deleteshangpin: function() {
					alert("删除中");
				},
				Settlement: function() {
					alert("结算中");
				},
				getTopData: function(){
					axios.get(domeurl+'/data/topData.json').then((response)=>{
						console.log(response.data);
		                this.Suspension_menu_listL = response.data.Suspension_menu_listL;
		                this.hd_menu_list = response.data.hd_menu_list;
		                this.Words_list = response.data.Words_list;
		                this.Navigation_name_list = response.data.Navigation_name_list;
		           	}).catch((response)=>{
		                console.log(response);
		                alert("获取tsTop初始数据失败");
		            })
				},
				getMenu_listData: function(){
					axios.get(domeurl+'/data/new_file.json').then((response)=>{
						console.log(response.data);
		                this.Menu_listData = response.data;
		           	}).catch((response)=>{
		                console.log(response);
		                alert("获取tsTop初始数据失败");
		            })
				},
				getID: function(){
					axios.get(domeurl+'/data/Data.json').then((response)=>{
						console.log(response.data.DataList[0]);
		                this.shangpingData = response.data.DataList[0];
		           	}).catch((response)=>{
		                console.log(response);
		                alert("获取初始数据失败");
		            })
				}
			}
		},
		"shIndex": {
			template: '#index',
			data: function() { //tada必须是一个方法，其返回值为obj对象
				return {
					name: "sh",
				};
			},
			created: function() {
				console.log("创建index之后");
				if (window.location.hash){
					this.type = (window.location.hash).substring(1);
				}
			},
			methods: {
				
			}
		},
		"shBottom": {
			template: '#bottom',
			data: function() { //tada必须是一个方法，其返回值为obj对象
				return {
					name: "bottom",
					index_styleData:null,
					clearfixData:null,
					text_linkData:null,
					dataWQ:null
				};
			},
			created: function() {
				console.log("创建shBottom之后");
				axios.get(domeurl+'/data/bottomData.json').then((response)=>{
					console.log(response.data);
	                this.index_styleData = response.data.index_styleData;
	                this.clearfixData = response.data.clearfixData;
	                this.text_linkData = response.data.text_linkData;
	                this.dataWQ = response.data.dataWQ;
	           	}).catch((response)=>{
	                console.log(response);
	                alert("获取shBottom初始数据失败");
	            })
			}
		},
		"shXiangqing": {
			template: '#xiangqing',
			data: function() { //tada必须是一个方法，其返回值为obj对象
				return {
					xqData:[],
					imgsrc: "",
					leftpx: 0
				};
			},
			created: function() {
				console.log("创建shXiangqing之后");
				this.getID();
			},
			mounted: function() {
				this.htmlFun();
				
			},
			methods: {
				htmlFun:function(){
					$('#showbox>div').mousemove(function(e){
						var xx = e.offsetX ; 
						var yy = e.offsetY ; 
						if (xx < 50) {xx = 50;} else if(xx > 350){xx = 350;}
						if (yy < 50) {yy = 50;} else if(yy > 500){yy = 500;}
						$('#zhezhao').css({
							left: (xx-50)+'px',
							top: (yy-50)+'px',
							display: 'block'
						});
						$("#showbox>p").css({
							"display":"block"
						});
						if (xx > 250) {xx = 250;}
						if (yy > 350) {yy = 350;}
						$("#showbox>p>img").css({
							"left": -((xx-50)*2)+"px",
							"top": -((yy-50)*2)+"px"
						});
					});
					$('#showbox>div').mouseout(function(e){
						$("#showbox>p").css({
							"display": "none"
						});
						$('#zhezhao').css({
							"display": "none"
						});
					});
				},
				getID: function(){
					axios.get(domeurl+'/data/Data.json').then((response)=>{
						console.log(response.data.DataList[0]);
		                this.xqData = response.data.DataList[0];
		                this.imgsrc = this.xqData.srcArr[0].src;
		           	}).catch((response)=>{
		                console.log(response);
		                alert("获取初始数据失败");
		            })
				},
				setimgID:function(v){
					this.imgsrc = this.xqData.srcArr[v].src;
				},
				clickLeft:function(v){
					if(v < 0 ){
						if ((this.leftpx - 62) < -69) {
							this.leftpx = -69;
						} else{
							this.leftpx -= 62;
						}
					} else if(v > 0){
						if ((this.leftpx + 62) > 0) {
							this.leftpx = 0;
						} else{
							this.leftpx += 62;
						}
					}
				}
			}
		}
	}
});